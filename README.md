## MixGoCloud
Get information about an artist, mix, using [MixCloud](https://mixcloud.com/)

### MixCloud API reference
[here](https://mixcloud.com/developers)

### Usage
For now, you can only get infos about artist and mixes

Compile using golang compiler (*NOT GCCGO!*)

~~~
$ go build *.go
~~~

IMPORTANT: libgo is required in order to run this. 

Use it:
~~~
$ ./<execname> <artist> [mix] #gets info about the <artist>'s [mix]
~~~

~~~
$ ./<execname> <artist> #gets info about <artist>
~~~

You must specify *[artist]* at least and _[mix]_ cannot be alone.


