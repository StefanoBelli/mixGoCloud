package main

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"os"
)

/* Final */
type mapData map[string]interface{}

func (mData mapData) String() string {
	var (
		finalString bytes.Buffer
		obj         string
	)

	for key, value := range mData {
		if value == "" || value == nil || value == float64(-1) {
			delete(mData, key)
		} else {
			obj = fmt.Sprintf("* %s %v\n", key, mData[key])
			if obj != "\n" {
				finalString.WriteString(obj)
			}
		}
	}

	return finalString.String()
}

/* ---- */

/* Util */
func sendBoolComm(comm chan bool, value bool) {
	comm <- value
}

/* Contact Mixcloud and get JSON-parsable text */
func getResponse(artist, mix string, commChan chan bool) (*http.Response, error) {

	var (
		completeURL string
		fmtErr      string
	)

	const MIXCLOUDAPI = "http://api.mixcloud.com"

	/* Actually, this is useless check but...*/
	if artist == "" {
		return nil, errors.New("err[getResponse]: No artist specified")
	} else if artist == "" && mix == "" {
		return nil, errors.New("err[getResponse]: No artist and mix specified")
	}

	/* Workaround avoiding segfault */
	if mix == "null" {
		completeURL = fmt.Sprintf("%s/%s", MIXCLOUDAPI, artist)
		go sendBoolComm(commChan, false)
	} else {
		completeURL = fmt.Sprintf("%s/%s/%s", MIXCLOUDAPI, artist, mix)
		go sendBoolComm(commChan, true)
	}

	respPtr, err := http.Get(completeURL)

	if err != nil {
		fmtErr = fmt.Sprintf("err[getResponse]: %s\n", err)
		return nil, errors.New(fmtErr)
	}

	return respPtr, nil
}

func cmdLineParser(args []string) (string, string) {
	requiredArgs := os.Args[1:]
	lenArgs := len(requiredArgs)

	if lenArgs < 1 || lenArgs > 2 {
		fmt.Fprintf(os.Stderr, "Error: Must specify <artist> and <mix>\n<artist> at least\n")
		os.Exit(1)
	}

	if lenArgs == 1 {
		return requiredArgs[0], "null"
	}

	return requiredArgs[0], requiredArgs[1]
}

func createMap(sData *fetchData) mapData {
	if sData == nil {
		return nil
	}

	var mData mapData // map in which data are stored
	mData = make(mapData, 15)

	mData["Artist Username:"] = sData.artistUsername
	mData["Artist Biography:"] = sData.artistBio
	mData["Artist City:"] = sData.artistCity
	mData["Artist Followers:"] = sData.artistFollowers
	mData["Artist Following:"] = sData.artistFollowing
	mData["Artist Uploads:"] = sData.artistUploadsCount
	mData["Artist URL:"] = sData.artistURL
	mData["Mix Name:"] = sData.mixFullName
	mData["Mix Favourites:"] = sData.mixFav
	mData["Mix Listener:"] = sData.mixListener
	mData["Mix URL:"] = sData.mixURL
	mData["Mix Uploaded:"] = sData.mixUploaded
	mData["Mix Reposted:"] = sData.mixReposted
	mData["Mix Desc:"] = sData.mixDesc
	mData["Mix Play:"] = sData.mixPlay

	return mData
}

func banner(dMap mapData) {
	fmt.Printf("MixGoCloud\n%s\n", dMap)
}

func main() {
	var (
		reqArgArtist string
		reqArgMix    string
		mcresp       *http.Response
		err          error
		isMix        chan bool
		cmdArgs      []string
		theData      *fetchData
		theMap       mapData
	)

	cmdArgs = os.Args
	reqArgArtist, reqArgMix = cmdLineParser(cmdArgs)

	if reqArgMix != "null" {
		/* returns []byte */
		bReqArgMix := bytes.Replace([]byte(reqArgMix), []byte(" "), []byte("-"), -1) /* Replaces spaces w/ - */
		bReqArgMix = bytes.ToLower(bReqArgMix)                                       /* characters are now lower */
		reqArgMix = string(bReqArgMix)                                               /* from []byte (bytes) to string */
		/* fmt.Printf("New String: %s\n", reqArgMix) */ /* testing purposes */
	}

	isMix = make(chan bool, 1)
	mcresp, err = getResponse(reqArgArtist, reqArgMix, isMix)

	if mcresp == nil && err != nil {
		fmt.Fprintf(os.Stderr, "Something wrong happend!\n%s\n", err)
		os.Exit(2)
	}

	theData = parseData(mcresp, isMix)
	close(isMix)
	mcresp.Body.Close()

	if theData == nil {
		fmt.Fprintln(os.Stderr, "Artist / Mix Does Not Exist!")
		os.Exit(1)
	}

	theMap = createMap(theData)

	banner(theMap)
}
