package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type fetchData struct {
	/* Artist related things */
	artistUsername     string
	artistCity         string
	artistFollowers    float64
	artistFollowing    float64
	artistUploadsCount float64
	artistBio          string
	artistURL          string
	/* Mixes */
	mixFullName string
	mixFav      float64
	mixListener float64
	mixURL      string
	mixUploaded string
	mixReposted float64
	mixDesc     string
	mixPlay     float64
}

func parseData(resHTTP *http.Response, commChan chan bool) *fetchData {
	var (
		hData    map[string]interface{}
		hDataSub map[string]interface{}
		pMix     bool
		data     *fetchData
	)

	readHTTPRes, err := ioutil.ReadAll(resHTTP.Body)

	if err != nil {
		panic(err)
	}

	pMix = <-commChan

	jerr := json.Unmarshal(readHTTPRes, &hData)

	if jerr != nil {
		panic(jerr)
	}

	if hData["key"] == nil {
		return nil
	}

	if !pMix {
		data = &fetchData{
			artistUsername:     hData["username"].(string),
			artistFollowers:    hData["follower_count"].(float64),
			artistFollowing:    hData["following_count"].(float64),
			artistUploadsCount: hData["cloudcast_count"].(float64),
			artistURL:          hData["url"].(string),
			artistCity:         "",
			artistBio:          "",
			mixFullName:        "",
			mixUploaded:        "",
			mixFav:             -1,
			mixReposted:        -1,
			mixDesc:            "",
			mixPlay:            -1,
			mixURL:             "",
			mixListener:        -1,
		}

		if hData["city"] != nil {
			data.artistCity = hData["city"].(string)
		}

		if hData["biog"] != nil {
			data.artistBio = hData["biog"].(string)
		}

	} else {
		hDataSub = make(map[string]interface{}, 7)
		hDataSub = hData["user"].(map[string]interface{})
		data = &fetchData{
			mixDesc:            "",
			mixFav:             hData["favorite_count"].(float64),
			mixFullName:        hData["name"].(string),
			mixListener:        hData["listener_count"].(float64),
			mixPlay:            hData["play_count"].(float64),
			mixReposted:        hData["repost_count"].(float64),
			mixURL:             hData["url"].(string),
			mixUploaded:        hData["created_time"].(string),
			artistUsername:     hDataSub["username"].(string),
			artistURL:          hDataSub["url"].(string),
			artistBio:          "",
			artistCity:         "",
			artistFollowers:    -1,
			artistFollowing:    -1,
			artistUploadsCount: -1,
		}

		if hData["description"] != nil {
			data.mixDesc = hData["description"].(string)
		}

	}

	return data
}
